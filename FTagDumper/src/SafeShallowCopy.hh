#ifndef SAFE_SHALLOW_COPY_HH
#define SAFE_SHALLOW_COPY_HH

#include "xAODCore/ShallowCopy.h"

#include <memory>


namespace xAOD {
  template <typename T>
  std::pair<std::unique_ptr<T>, std::unique_ptr<ShallowAuxContainer>>
  safeShallowCopyContainer(const T& cont) {
    auto unsafe = shallowCopyContainer(cont);
    return {
      std::unique_ptr<T>(unsafe.first),
      std::unique_ptr<ShallowAuxContainer>(unsafe.second)
    };
  }
}

#endif
