FROM gitlab-registry.cern.ch/atlas-sit/docker/alma9-atlasos-dev

# local and envs
ENV PIP_ROOT_USER_ACTION=ignore
ARG DEBIAN_FRONTEND=noninteractive
USER root

# add some packages
RUN yum install -y git h5utils wget vim build-essential

# setup
WORKDIR /tdd
COPY . /tdd/training-dataset-dumper

# build the code
RUN mkdir /tdd/build && \
    cd /tdd/build && \
    source /release_setup.sh && \
    cmake ../training-dataset-dumper && \
    make

# run this when starting the container
CMD source /release_setup.sh && source /tdd/build/x*/setup.sh && /bin/bash
